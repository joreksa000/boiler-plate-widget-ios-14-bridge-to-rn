//
//  SynergyWidget.swift
//  SynergyWidget
//
//  Created by Joda Reksa on 14/01/23.
//

import WidgetKit
import SwiftUI

struct Provider: TimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date())
    }

    func getSnapshot(in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date())
        completion(entry)
    }

    func getTimeline(in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
}

struct SynergyWidgetEntryView : View {
  var entry: Provider.Entry
         
         @Environment(\.widgetFamily) var family //<- here
         
         @ViewBuilder
         var body: some View {
             switch family {
             case .systemSmall:
                 Text("")
             default:
               Color.black.overlay(
               HStack(){
                     Link(destination: URL(string: "widget-deeplink://Home")!, label: {
                            VStack(alignment: .center){
                              Circle()
                                  .fill(.gray)
                                  .frame(width: 50, height: 50)
                                   Text("page1")
                                   .font(.system(size:10))
                                   .bold()
                                   .foregroundColor(Color.white)
                       }
                     })
                     Spacer()
                     Link(destination: URL(string: "widget-deeplink://WidgetScreen")!, label: {
                       VStack(alignment: .center){
                         Circle()
                             .fill(.gray)
                             .frame(width: 50, height: 50)
                              Text("page2")
                              .font(.system(size:10))
                              .bold()
                              .foregroundColor(Color.white)
                  }
                     })
                   Spacer()
                   Link(destination: URL(string: "widget-deeplink://Notifications")!, label: {
                         VStack(alignment: .center){
                           Circle()
                               .fill(.gray)
                               .frame(width: 50, height: 50)
                                Text("page3")
                                .font(.system(size:10))
                                .bold()
                                .foregroundColor(Color.white)
                }
                   })
                   Spacer()
                   Link(destination: URL(string: "widget-deeplink://Settings")!, label: {
                         VStack(alignment: .center){
                           Circle()
                               .fill(.gray)
                               .frame(width: 50, height: 50)
                                Text("page4")
                                .font(.system(size:10))
                                .bold()
                                .foregroundColor(Color.white)
                }
                   })
                 }
               
                 .padding()
                 )
             }
               
         }
               
}

@main
struct SynergyWidget: Widget {
    let kind: String = "SynergyWidget"

    var body: some WidgetConfiguration {
        StaticConfiguration(kind: kind, provider: Provider()) { entry in
            SynergyWidgetEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
}

struct SynergyWidget_Previews: PreviewProvider {
    static var previews: some View {
      VStack(){
        SynergyWidgetEntryView(entry: SimpleEntry(date: Date()))
      }
        .previewContext(WidgetPreviewContext(family: .systemMedium))
          
    }
}
