//
//  SynergyWidgets.swift
//  SynergyWidgets
//
//  Created by Joda Reksa on 14/01/23.
//

import WidgetKit
import SwiftUI
import Intents

struct Provider: IntentTimelineProvider {
    func placeholder(in context: Context) -> SimpleEntry {
        SimpleEntry(date: Date(), configuration: ConfigurationIntent())
    }

    func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
        let entry = SimpleEntry(date: Date(), configuration: configuration)
        completion(entry)
    }

    func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        var entries: [SimpleEntry] = []

        // Generate a timeline consisting of five entries an hour apart, starting from the current date.
        let currentDate = Date()
        for hourOffset in 0 ..< 5 {
            let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
            let entry = SimpleEntry(date: entryDate, configuration: configuration)
            entries.append(entry)
        }

        let timeline = Timeline(entries: entries, policy: .atEnd)
        completion(timeline)
    }
}

struct SimpleEntry: TimelineEntry {
    let date: Date
    let configuration: ConfigurationIntent
}

struct SynergyWidgetsEntryView : View {
  var entry: Provider.Entry

     var body: some View {
       VStack(){
         HStack(alignment:.center) {
           VStack(alignment: .center){
             Image("contact")
                 .resizable()
                 .aspectRatio(contentMode: .fit)
                 .frame(width: 50, height: 50, alignment: .trailing )
                 .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                 .overlay(
                  Circle().stroke(Color.black))
             Text("page1")
             .font(.system(size:10))
             .bold()
             .widgetURL(URL(string: "widget-deeplink://WidgetScreen"))
           
           }
           VStack(alignment: .center){
             Image("contact")
                 .resizable()
                 .aspectRatio(contentMode: .fit)
                 .frame(width: 50, height: 50, alignment: .trailing )
                 .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                 .overlay(
                  Circle().stroke(Color.blue))
             Text("page2")
             .font(.system(size:10))
             .bold()
             .widgetURL(URL(string: "widget-deeplink://Widget2"))
           }
           VStack(alignment: .center){
             Image("contact")
                 .resizable()
                 .aspectRatio(contentMode: .fit)
                 .frame(width: 50, height: 50, alignment: .trailing )
                 .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                 .overlay(
                  Circle().stroke(Color.black))
             Text("page3")
             .font(.system(size:10))
             .bold()
             .widgetURL(URL(string: "widget-deeplink://WidgetScreen"))
           }
           VStack(alignment: .center){
             Image("contact")
                 .resizable()
                 .aspectRatio(contentMode: .fit)
                 .frame(width: 50, height: 50, alignment: .trailing )
                 .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                 .overlay(
                  Circle().stroke(Color.black))
             Text("page4")
             .font(.system(size:10))
             .bold()
             .widgetURL(URL(string: "widget-deeplink://WidgetScreen"))
           }
                  
           }
       }
       
         
     }
}

@main
struct SynergyWidgets: Widget {
    let kind: String = "SynergyWidgets"

    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: ConfigurationIntent.self, provider: Provider()) { entry in
            SynergyWidgetsEntryView(entry: entry)
        }
        .configurationDisplayName("My Widget")
        .description("This is an example widget.")
    }
  
}

struct SynergyWidgets_Previews: PreviewProvider {
    static var previews: some View {
        SynergyWidgetsEntryView(entry: SimpleEntry(date: Date(), configuration: ConfigurationIntent()))
            .previewContext(WidgetPreviewContext(family: .systemMedium))
    }
}
