import 'react-native-gesture-handler';
import React from 'react'
import { Button, View,NativeModules } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Page2"
        onPress={() => navigation.navigate('WidgetScreen')}
      />
    </View>
  );
}

function WidgetScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Page3"
        onPress={() => navigation.navigate('Notifications')}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Page 4"
        onPress={() => navigation.navigate('Settings')}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

function SettingsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

const Stack = createStackNavigator();

function MyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Page 1' }} />
      <Stack.Screen name="Notifications" component={NotificationsScreen} options={{ title: 'Page 3' }} />
      <Stack.Screen name="WidgetScreen" component={WidgetScreen} options={{ title: 'Page 2' }}/>
      <Stack.Screen name="Settings" component={SettingsScreen} options={{ title: 'Page 4' }}/>
    </Stack.Navigator>
  );
}

export default function App() {
  return (
    <NavigationContainer
      linking={{
        prefixes: [
          'widget-deeplink://',
        ],
        config: {
          screens: {
            Home:'Home',
            WidgetScreen: 'WidgetScreen',
            Notifications:'Notifications',
            Settings:'Settings'
          },
        }
      }}>
      <MyStack />
    </NavigationContainer>
  );
}
